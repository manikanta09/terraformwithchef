# Terraform
 * Terraform is used to create infrastructure.
 * Terraform has its own Domain Specific Language.
 * File extension must be .tf 

## Terraform Building Blocks
 * Provider
 * Resource
 * Variables

# Chef
 * Chef is a powerful deployment tool.
 * Chef automates how infrastructure is configured, deployed, and managed across your network, no matter its size.
# Now let’s go ahead and setup chef Server, Workstation, and Node (Chef Client).

## Environment Details
 * I am using 3 Ubuntu  systems. One will act as a chef server, and the next one will be a workstation, and a 3rd system will be the node.

# Chef Server installation
 * Now, let us run the command below to download the chef-server package.
  ```
    "sudo apt-get -y update" 
  ```
# to download the chef-server package,
  ``` 
    "wget https://packages.chef.io/files/stable/chef-server/12.18.14/ubuntu/18.04/chef-server-core_12.18.14-1_amd64.deb" 
 ```
# to install chef-server
```
    "sudo dpkg -i chef-server-core_*.deb",
```
# to start chef server 
    "sudo chef-server-ctl reconfigure" 
    "sudo apt-get install openssl -y",
# Create the SSL Certificate
```
    "openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/ca.key -out /etc/ssl/certs/ca.crt"  
```
# create  an admin user
```
    "sudo chef-server-ctl user-create chefadmin Chef Admin chefadmin@gmail.com 'admin@123' --filename ./chfsrv.pem" 
```
# to view the list of users
    chef-server-ctl user-list
# create an organization
```
    "sudo chef-server-ctl org-create VBI 'vbi Pvt Ltd.' --association_user chefadmin --filename ./org.pem" ,
```
# to view the list of organizations
    chef-server-ctl org-list      
# to install the management console
```
    "sudo chef-server-ctl install chef-manage",
    "sudo chef-server-ctl reconfigure --accept-license",
    "sudo chef-manage-ctl reconfigure --accept-license",
```
# to install reporting feature
```
    "sudo chef-server-ctl install opscode-reporting",
    "sudo chef-server-ctl reconfigure",
    "sudo opscode-reporting-ctl reconfigure --accept-license"
``` 
# Chef Workstation
```
    "sudo apt-get -y update"
```
# to download the chef-workstatiion package
```
    "wget  https://packages.chef.io/files/stable/chef-workstation/0.2.43/ubuntu/18.04/chef-workstation_0.2.43-1_amd64.deb",
```
# to install chef-workstation
```
    "sudo dpkg -i chef-workstation_*.deb",
```
# to create a chef repo 
    "chef generate repo chef-repo"
    

 ## Terraform Execution Steps
```
    $ terraform init
    $ terraform validate
    $ terraform apply
    $ terraform show
```
##  Note that this example may create resources which can cost money. Run terraform destroy when you don't need these resources.
  $ terraform destroy