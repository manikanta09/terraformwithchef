resource "aws_vpc" "webser" {
  cidr_block       = "10.10.0.0/16"
 
}
resource "aws_subnet" "subnet-1" {
  cidr_block        = "10.10.0.0/24"
  vpc_id             = aws_vpc.webser.id
  availability_zone = var.subnet1az
  
}

resource "aws_internet_gateway" "my_igw" {
    vpc_id      = aws_vpc.webser.id
   
}

resource "aws_route_table" "my_rt" {
    vpc_id = aws_vpc.webser.id
    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = aws_internet_gateway.my_igw.id
    }
 
}

resource "aws_security_group" "my_sg" {
     
    vpc_id          = aws_vpc.webser.id
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "80"
        to_port     = "80"
    }
    ingress {
    # chef default pport 
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    
  }

    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}

resource "aws_route_table_association" "subnet1assoc" {
    subnet_id       = aws_subnet.subnet-1.id
    route_table_id  = aws_route_table.my_rt.id
  
}


resource "aws_instance" "chefserver" {
    ami                         =  var.chef
    instance_type               = "t2.large"
    subnet_id                   = aws_subnet.subnet-1.id
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        password    = "admin@123"
        private_key = file(var.sshkeypath)
        host        = aws_instance.chefserver.public_ip
    }

     provisioner "remote-exec" {
    inline = [
          "sudo apt-get -y update" ,
          # to download the chef-server package,
          "wget https://packages.chef.io/files/stable/chef-server/12.18.14/ubuntu/18.04/chef-server-core_12.18.14-1_amd64.deb" ,
          # to install chef-server
          "sudo dpkg -i chef-server-core_*.deb",
          #to start chef server 
          "sudo chef-server-ctl reconfigure" ,
          "sudo apt-get install openssl -y",
          # Create the SSL Certificate
          "openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/ca.key -out /etc/ssl/certs/ca.crt",
         
          # create  an admin user
          "sudo chef-server-ctl user-create chefadmin Chef Admin chefadmin@gmail.com 'admin@123' --filename ./chfsrv.pem" ,
          # to view the list of users
          # chef-server-ctl user-list
          # create an organization
          "sudo chef-server-ctl org-create VBI 'vbi Pvt Ltd.' --association_user chefadmin --filename ./org.pem" ,
        # to view the list of organizations
        # chef-server-ctl org-list
        
          # to install the management console
            "sudo chef-server-ctl install chef-manage",
            "sudo chef-server-ctl reconfigure --accept-license",
            "sudo chef-manage-ctl reconfigure --accept-license",
         # to install reporting feature
          "sudo chef-server-ctl install opscode-reporting",
          "sudo chef-server-ctl reconfigure",
          "sudo opscode-reporting-ctl reconfigure --accept-license"
    ]
  }
   tags = {
    Name = "chef-server"
  }
}
resource "aws_instance" "chefworkstation" {
    ami                         =  var.chef
    instance_type               = "t2.micro"
    subnet_id                   = aws_subnet.subnet-1.id
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.chefworkstation.public_ip
    }

     provisioner "remote-exec" {
    inline = [
          "sudo apt-get -y update",
          # to download the chef-workstatiion package
         #"curl -L https://www.opscode.com/chef/install.sh | sudo bash" ,
          "wget  https://packages.chef.io/files/stable/chef-workstation/0.2.43/ubuntu/18.04/chef-workstation_0.2.43-1_amd64.deb",
          # to install chef-workstation
          "sudo dpkg -i chef-workstation_*.deb",
          #to create a chef repo 
          "chef generate repo chef-repo",

    ]
  }
   tags = {
    Name = "chef-workstation"
  }
}
resource "aws_instance" "chefclient" {
    ami                         =  var.chef
    instance_type               = "t2.micro"
    subnet_id                   = aws_subnet.subnet-1.id
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.chefclient.public_ip
    }

     
   tags = {
    Name = "chef-client"
  }
}
output "chef1-ip" {
  value = aws_instance.chefserver.public_ip
}

output "chef2-ip" {
  value = aws_instance.chefworkstation.public_ip
}
output "chef3-ip" {
  value = aws_instance.chefclient.public_ip
}