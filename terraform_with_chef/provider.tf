provider "aws" {    
   region     =  var.awsreg
  access_key =   var.accesskey
  secret_key =   var.secretkey
}
